﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Developertest.ApplicationService.ApplicationService.Interface;
using Agl.Developertest.ApplicationService.Core.Service;
using Agl.Developertest.ApplicationService.CoreService;
using Agl.Developertest.ApplicationService.DomainModel;

namespace Agl.Developertest.ApplicationService.ApplicationService
{
    public class AglDeveloperTestApllicationService : IAglDeveloperTestApllicationServiceInterface
    {
        private string _jsonurl;

        public AglDeveloperTestApllicationService(string jsonurl)
        {
            this._jsonurl = jsonurl;
        }

        public Persons GetAllPersons()
        {
            try
            {
                var httpclient = new AglHttpClient();
                var persons = httpclient.GetDataAsync(new Uri(_jsonurl)).Result.Content.ReadAsStringAsync();
                var json = new StringBuilder();
                json.Append("{'persons':");
                json.Append(persons.Result);
                json.Append("}");
                return Serializer<Persons>.Deserialize(json.ToString());
            }
            catch (Exception ex)
            {
                //log the exception
               return null;
            }
           
        }

        public List<string> ListAllCatsNamesWithFemaleOwner()
        {
            var persons = GetAllPersons();
            if (persons == null) return null;
            var femaleOwners = persons.persons.Where(x => x.gender == "Female").ToList();
            var catnamesFemale = (from female in femaleOwners.Where(female => female != null && female.pets != null)
                from pet in female.pets.Where(pet => pet.type == "Cat").ToList()
                select pet.name).ToList();
            catnamesFemale.Sort();
            return catnamesFemale;
        }

        public List<string> ListAllCatsNamesWithMaleOwner()
        {
            var persons = GetAllPersons();
            if (persons == null) return null;
            var maleOwners = persons.persons.Where(x => x.gender == "Male").ToList();
            var catnamesMale = (from male in maleOwners.Where(male => male != null && male.pets != null)
                                from pet in male.pets.Where(pet => pet.type == "Cat").ToList()
                                select pet.name).ToList();
            catnamesMale.Sort();
            return catnamesMale;
        }

    }
}
