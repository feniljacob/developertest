﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Developertest.ApplicationService.DomainModel;

namespace Agl.Developertest.ApplicationService.ApplicationService.Interface
{
    interface IAglDeveloperTestApllicationServiceInterface
    {
        Persons GetAllPersons();
        List<string> ListAllCatsNamesWithFemaleOwner();

        List<string> ListAllCatsNamesWithMaleOwner();

    }
}
