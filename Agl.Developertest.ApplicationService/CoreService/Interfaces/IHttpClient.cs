﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Agl.Developertest.ApplicationService.Core.Service.Interfaces
{
    public interface IHttpClient : IDisposable
    {
        Task<HttpResponseMessage> GetDataAsync(Uri url);
    }
}
