﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Agl.Developertest.ApplicationService.CoreService
{
    public class Serializer<T>
    {
        #region Private Fields

        private static readonly JsonSerializerSettings _settings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Formatting = Formatting.Indented

        };

        #endregion

        #region Constructor

        static Serializer()
        {
            _settings.Converters.Add(new StringEnumConverter());
        }

        #endregion

        #region Public Methods

        public static string Serialize(T t)
        {
            return JsonConvert.SerializeObject(t, _settings);
        }

        public static T Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _settings);
        }

        #endregion
    }
}
