﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Agl.Developertest.ApplicationService.Core.Service.Interfaces;

namespace Agl.Developertest.ApplicationService.CoreService
{
    public class AglHttpClient : IHttpClient
    {
        #region Private Fields

        private readonly HttpClient _httpClient;

        #endregion

        #region Constructor

        public AglHttpClient()
        {
            _httpClient = new HttpClient();
        }

        #endregion

        #region IHttpClient Members

        public Task<HttpResponseMessage> GetDataAsync(Uri uri)
        {
            var httpResponseMessage = _httpClient.GetAsync(uri);

            if (httpResponseMessage == null)
            {
                throw new Exception("Get async error - Http response message is not returned any data.");
            }
            return httpResponseMessage;
        }
        public void Dispose()
        {
            _httpClient.Dispose();
        }

        #endregion
    }
}
