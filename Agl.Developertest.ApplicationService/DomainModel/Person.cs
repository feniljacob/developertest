﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agl.Developertest.ApplicationService.DomainModel
{
    public class Pet:IComparable<Pet>
    {
        public string name { get; set; }
        public string type { get; set; }
        public int CompareTo(Pet other)
        {
            // A null value means that this object is greater.
            if (other == null)
                return 1;

            else
                return string.Compare(this.name, other.name, StringComparison.Ordinal);
        }
    }

    public class Person
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public IList<Pet> pets { get; set; }
    }

    public class Persons
    {
        public IList<Person> persons { get; set; }
    }
}
