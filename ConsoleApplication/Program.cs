﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Developertest.ApplicationService.ApplicationService;
using Agl.Developertest.ApplicationService.DomainModel;

namespace ConsoleApplication
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var service = new AglDeveloperTestApllicationService("http://agl-developer-test.azurewebsites.net/people.json");
            Console.WriteLine("Male");
            var catnameswithMaleOwner= service.ListAllCatsNamesWithMaleOwner();
            if (catnameswithMaleOwner != null)
            {
                foreach (var name in catnameswithMaleOwner)
                {
                    Console.WriteLine(name);
                }
            }

            Console.WriteLine("Female");
            var catnameswithFemaleOwner = service.ListAllCatsNamesWithFemaleOwner();
            if (catnameswithFemaleOwner != null)
            {
                foreach (var name in catnameswithFemaleOwner)
                {
                    Console.WriteLine(name);
                }
            }
        }
    }
}
