﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Agl.Developertest.ApplicationService;
using Agl.Developertest.ApplicationService.CoreService;
using Agl.Developertest.ApplicationService.DomainModel;
using Agl.Developertest.ApplicationService.ApplicationService;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestMethods
    {
        [TestMethod]
        public void Deserialize_PersonResponse()
        {

            string json =
                "{'persons':[{'name':'Bob','gender':'Male','age':23,'pets':[{'name':'Garfield','type':'Cat'},{'name':'Fido','type':'Dog'}]}]}";
            var deserializeResponse = Serializer<Persons>.Deserialize(json);

            Assert.IsTrue(deserializeResponse.persons.Count > 0 && deserializeResponse.persons[0].name == "Bob");
        }

        [TestMethod]
        public void Test_GetAllPersons()
        {
            var service = new AglDeveloperTestApllicationService("http://agl-developer-test.azurewebsites.net/people.json");
            var person = service.GetAllPersons();
            Assert.IsTrue(person != null && person.persons != null);

        }

        [TestMethod]
        public void Test_ListAllCatsNamesWithFemaleOwner()
        {
            var service = new AglDeveloperTestApllicationService("http://agl-developer-test.azurewebsites.net/people.json");
            var names = service.ListAllCatsNamesWithFemaleOwner();
            Assert.IsTrue(names != null && names.Contains("Tabby"));

        }

        [TestMethod]
        public void Test_ListAllCatsNamesWithMaleOwner()
        {
            var service = new AglDeveloperTestApllicationService("http://agl-developer-test.azurewebsites.net/people.json");
            var names = service.ListAllCatsNamesWithMaleOwner();
            Assert.IsTrue(names != null && names.Contains("Garfield"));

        }
    }
}
