﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    public class CatNamesViewModel
    {
        public List<string> CatnamesWithFemaleOwner { get; set; }
        public List<string> CatnamesWithMaleOwner { get; set; }
    }
}
