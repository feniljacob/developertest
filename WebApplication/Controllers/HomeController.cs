﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Agl.Developertest.ApplicationService.ApplicationService;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var service = new AglDeveloperTestApllicationService("http://agl-developer-test.azurewebsites.net/people.json");
            var model = new CatNamesViewModel
            {
                CatnamesWithFemaleOwner = service.ListAllCatsNamesWithFemaleOwner(),
                CatnamesWithMaleOwner = service.ListAllCatsNamesWithMaleOwner()
            };
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}